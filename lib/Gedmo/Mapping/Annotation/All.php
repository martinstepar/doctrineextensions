<?php

/**
* Contains all annotations for extensions
* NOTE: should be included with require_once
*
* @author Gediminas Morkevicius <gediminas.morkevicius@gmail.com>
* @license MIT License (http://www.opensource.org/licenses/mit-license.php)
*/

require_once __DIR__.'/Language.php';
require_once __DIR__.'/Locale.php';
require_once __DIR__.'/Loggable.php';
require_once __DIR__.'/Slug.php';
require_once __DIR__.'/SlugHandler.php';
require_once __DIR__.'/SlugHandlerOption.php';
require_once __DIR__.'/SoftDeleteable.php';
require_once __DIR__.'/SortableGroup.php';
require_once __DIR__.'/SortablePosition.php';
require_once __DIR__.'/Timestampable.php';
require_once __DIR__.'/Blameable.php';
require_once __DIR__.'/Translatable.php';
require_once __DIR__.'/TranslationEntity.php';
require_once __DIR__.'/Tree.php';
require_once __DIR__.'/TreeClosure.php';
require_once __DIR__.'/TreeLeft.php';
require_once __DIR__.'/TreeLevel.php';
require_once __DIR__.'/TreeLockTime.php';
require_once __DIR__.'/TreeParent.php';
require_once __DIR__.'/TreePath.php';
require_once __DIR__.'/TreePathSource.php';
require_once __DIR__.'/TreeRight.php';
require_once __DIR__.'/TreeRoot.php';
require_once __DIR__.'/Versioned.php';
require_once __DIR__.'/Uploadable.php';
require_once __DIR__.'/UploadableFileMimeType.php';
require_once __DIR__.'/UploadableFilePath.php';
require_once __DIR__.'/UploadableFileSize.php';
