<?php

namespace Gedmo\Mapping\Annotation;

use Doctrine\ORM\Mapping\Annotation;

/**
 * Group annotation for Sortable extension
 *
 * @author Lukas Botsch <lukas.botsch@gmail.com>
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * @Annotation
 * @Target("PROPERTY")
 */
final class SortableGroup implements Annotation
{

}
